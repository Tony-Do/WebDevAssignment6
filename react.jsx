// react.jsx
"use strict";

const { useState } = React;

const SearchForm = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [selectedArtwork, setSelectedArtwork] = useState(null);
  const handleSearchTermChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();

    const apiURL = `https://api.artic.edu/api/v1/artworks?limit=5&fields=id,title,image_id&q=${searchTerm}`;

    fetch(apiURL)
      .then((response) => {
        if (!response.ok) {
          throw new Error('The response did not work');
        }
        return response.json();
      })
      .then((data) => {
        setSearchResults(data.data);
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  };

  const handleItemClick = (artwork) => {
    setSelectedArtwork(artwork);
  }

  return (
    <div>
      <form onSubmit={handleSearchSubmit}>
        <label htmlFor="search">Artwork Title:</label>
        <input
          type="text"
          id="search"
          name="search"
          value={searchTerm}
          onChange={handleSearchTermChange}
        />
        <button type="submit">Search</button>
      </form>

      <div>
        <ul>
            {searchResults.map((artwork) => (
                <li
                key={artwork.id}
                onClick={() => handleItemClick(artwork)}
                className={selectedArtwork && selectedArtwork.id === artwork.id ? 'selected' : ''}
                >
                {artwork.title}
                </li>
            ))}
            </ul>
      </div>
    </div>
  );
};

ReactDOM.render(<SearchForm />, document.getElementById('react-root'));
