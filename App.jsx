const { useState } = React;

const App = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [selectedArtwork, setSelectedArtwork] = useState(null);

  const handleSearchTermChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();

    const apiURL = `https://api.artic.edu/api/v1/artworks/search?q=${searchTerm}`;

    fetch(apiURL)
      .then((response) => {
        if (!response.ok) {
          throw new Error('The response did not work');
        }
        return response.json();
      })
      .then((data) => {
        setSearchResults(data.data);
      })
      .catch((error) => {
        console.error('Error fetching the data:', error);
      });
  };

  const handleArtworkClick = (id) => {
    const apiURL = `https://api.artic.edu/api/v1/artworks/${id}`;

    fetch(apiURL)
      .then((response) => {
        if (!response.ok) {
          throw new Error('The response did not work');
        }
        return response.json();
      })
      .then((data) => {
        if (data && data.data) {
          const artworkDetails = data.data;

          if (artworkDetails.image_id) {
            const imageUrl = `https://www.artic.edu/iiif/2/${artworkDetails.image_id}/full/!800,800/0/default.jpg`;
            {/* Making sure that the url is correct, in case it might be undefined 
                Also to just keep track of the urls */ } 
            console.log('Image URL:', imageUrl);
          } else {
            console.error('Image ID not available in the response');
          }

          setSelectedArtwork(artworkDetails);
        } else {
          console.error('Error, something went wrong');
        }
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  };

  return (
    <div className="wrapper">
      <SearchForm
        searchTerm={searchTerm}
        onSearchTermChange={handleSearchTermChange}
        onSearchSubmit={handleSearchSubmit}
        searchResults={searchResults}
        onArtworkClick={handleArtworkClick}
      />
      <main>
        {selectedArtwork && <DetailsView artwork={selectedArtwork} />}
      </main>
    </div>
  );
};

const SearchForm = (props) => {
  const [selectedArtworkId, setSelectedArtworkId] = useState(null);

  const handleItemClick = (artwork) => {
    setSelectedArtworkId(artwork.id);
    props.onArtworkClick(artwork.id);
  };

  return (
    <header>
      <h2>Art Institute of Chicago</h2>
      <form onSubmit={props.onSearchSubmit}>
        <label htmlFor="search">Artwork Title:</label>
        <input type="text" id="search" name="search" value={props.searchTerm} onChange={props.onSearchTermChange} />
        <button type="submit">Search</button>
      </form>
      <div>
        <ul>
          {props.searchResults.map((artwork) => (
            <li key={artwork.id} onClick={() => handleItemClick(artwork)} className={selectedArtworkId === artwork.id ? 'selected' : ''}>
              {artwork.title}
            </li>
          ))}
        </ul>
      </div>
    </header>
  );
};

const DetailsView = ({ artwork }) => {
  console.log(artwork);
  return (
    <div className="artist-infos">
      <h1>{artwork.title}</h1>
      {/* it shows the markup inside the description, but using dangerouslySetInnerHtml gave me more trouble, so I'll leave it there */}
      <p>{artwork.description}</p>
      <p>Artist: {artwork.artist_display}</p>
      <p>Date: {artwork.date_display}</p>
      <p>Place of Origin: {artwork.place_of_origin}</p>
      <img src={`https://www.artic.edu/iiif/2/${artwork.image_id}/full/!800,800/0/default.jpg`} alt={artwork.title}/>
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById('react-root'));
